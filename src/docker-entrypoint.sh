#!/usr/bin/env bash
export PYTHONPATH="/app"

if [ "$1" == "run" ]; then
  python run.py
elif [ "$1" == "test" ]; then
  pytest -v -s --cov-report term-missing --cov=webex_teams_relay --basetemp={envtmpdir} tests
else
  exec $@
fi

