import json
import logging
import pika

# Local imports
from webex_teams_relay.webex_teams import WebexTeamsClient

LOG = logging.getLogger(__name__)


class MessageConsumer:

    def __init__(self, config):
        self.config = config
        self.webex_teams_client = WebexTeamsClient(self.config)

    def _on_message(self, channel, method_frame, header_frame, body):
        """
        The callback method to send the consumed message to webex_teams.

        :param channel:
        :param method_frame:
        :param header_frame:
        :param body: json string of message data
        """

        if method_frame:
            message = json.loads(body)
            LOG.info('message is {0}'.format(message))
            message_text = message.get('messagetext')
            markdown_text = message.get('markdowntext')
            self.webex_teams_client.send_message(
                message_text=message_text,
                markdown_text=markdown_text,
                room_name=message['roomid'])
        else:
            LOG.info('No messages found')

    def relay_messages(self):
        """
        Connect to the AMQP server and send start consuming messages.
        """

        credentials = pika.PlainCredentials(self.config['AMQP_USER'],
                                            self.config['AMQP_PASS'])
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=self.config['AMQP_HOST'], credentials=credentials))
        channel = connection.channel()
        channel.exchange_declare(exchange='webex_teams',
                                 exchange_type='fanout')
        result = channel.queue_declare(exclusive=True)
        queue_name = result.method.queue
        channel.queue_bind(exchange='webex_teams',
                           queue=queue_name)
        channel.basic_consume(self._on_message, queue=queue_name,
                              no_ack=True)
        try:
            channel.start_consuming()
        except KeyboardInterrupt:
            channel.stop_consuming()
        connection.close()
